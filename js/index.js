const ul = document.querySelector('.tabs');

ul.addEventListener('click', (event) => {
    const tabsTarget = event.target.getAttribute('data-tab');
    const currentText = document.getElementById(tabsTarget);
    const activeTab = ul.querySelector(".active");
    const activeTabData = activeTab.getAttribute("data-tab");
    const activeText = document.getElementById(activeTabData);
    activeTab.classList.remove("active");
    activeText.classList.add("hidden-tabs-content");
    event.target.classList.add("active");
    currentText.classList.remove("hidden-tabs-content");
})

    





